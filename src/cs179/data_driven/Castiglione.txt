Main courtyard
---
You enter the church and look around.
As you look around, a sparkle catches your eye.
You go over to it and see it's a fragment of key.
Pick it up? (y/n) -
---
You enter the kitchen and see what looks like a meal preparation cut short.
Pots are still left on the stove and the food has begun to grow mold.
You see a doorknob behind a shelf.
You push the shelf over and open the door to a dark staircase.
---
You enter the ruins and see nothing of interest around besides some broken furniture and the blowing wind.
Once again, a flash catches your eye and you see another fragment of a key.
Suddenly, you hear a faint, but very shrill scream followed by the sound of thudding footsteps
heading towards you from a dark staircase you hadn't seen previously.
Pick up the key? (y/n) -
---
You walk down the stairs into the cellar and light up a candle you found in the kitchen.
You see something glow in the dark, and quickly realize it's another fragment of a key.
Pick up the key? (y/n) -
---
You walk down the stairs into the dark dungeon.
The darkness is so thick it is almost suffocating.
You feel an ominous presence.